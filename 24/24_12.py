import itertools
from collections import Counter
from operator import mul

presents = []

def get_groups(conf_size, presents):
    groups = []
    for i in range(2, len(presents)):
        if groups:  # No need to loop larger groups if we already found something
            break
        for grp in itertools.combinations(presents, i):
            # Apparently for all given inputs we can just check the first group and the rest should match too...
            if sum(grp) == (sum(presents)/conf_size):
                groups.append(grp)
    return groups

def calculate_min_qe(groups):
    # Select the configurations that have the lowest amount of presents in group 1
    # and find the configuration that has the lower QE in group 1
    min_qe = reduce(mul, groups[0])
    #print group1
    for grp in groups[1:]:
        qe = reduce(mul, grp)
        min_qe = min(qe, min_qe)
    return min_qe

with open("input.txt") as data:
    presents = [int(line) for line in data.readlines()]

groups = get_groups(3,presents)
print "Smallest QE for Part 1: {}".format(calculate_min_qe(groups))

groups = get_groups(4,presents)
print "Smallest QE for Part 2: {}".format(calculate_min_qe(groups))
