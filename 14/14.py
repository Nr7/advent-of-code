class Reindeer:
    def __init__ (self, name, max_speed, fly_time, rest_time):
        self.name         = name
        self.max_speed    = max_speed
        self.fly_time     = fly_time
        self.rest_time    = rest_time
        self.time_elapsed = 0
        self.distance     = 0
        self.state        = "Fly"
        self.points       = 0

    def increment(self):
        self.time_elapsed += 1
        if self.state == "Fly":
            self.distance += self.max_speed
            if self.time_elapsed >= self.fly_time:
                self.state = "Rest"
                self.time_elapsed = 0
        else:
            if self.time_elapsed >= self.rest_time:
                self.state = "Fly"
                self.time_elapsed = 0
        return self.distance

    def add_point(self):
        self.points += 1

    def __str__(self):
        return self.name + " flew " + str(self.distance) + " km and got " + str(self.points) + " points"

reindeers = []
# Parse reindeers from file
with open('input.txt') as input:
    for line in input.readlines():
        line = line.split(" ")
        rd = Reindeer(line[0], int(line[3]), int(line[6]), int(line[-2]))
        reindeers.append(rd)

time = 2503 #1000
# Calculate distance traveled in <time> seconds
for i in range(0, time):
    furthest = -1 
    for rd in reindeers:
        distance = rd.increment()
        if distance > furthest:
            furthest = distance
    # Award points to the reindeer who are furthest
    for rd in reindeers:
        if rd.distance == furthest:
            rd.add_point()



for rd in reindeers:
    print rd

