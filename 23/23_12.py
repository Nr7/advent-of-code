def hlf(r):
    return r / 2

def tpl(r):
    return r * 3

def inc(r):
    return r + 1

def jmp(offset, i):
    return i + offset

def jie(r, offset, i):
    if (r % 2) == 0:
        return i + offset
    return i + 1

def jio(r, offset, i):
    if r == 1:
        return i + offset
    return i + 1

a = 1
b = 0
with open('input.txt') as data:
    lines = data.readlines()
    i = 0
    while i < len(lines):
        print "a: {}, b: {}, i: {}".format(a,b,i)
        print "instruction: " + lines[i]
        line = lines[i].rstrip("\n").split(" ")
        instruction = line[0]
        if instruction == "jmp":
            i = jmp(int(line[1].lstrip("+")), i)
            continue
        else:
            if line[1].rstrip(",") == "a":
                reg = a
            elif line[1].rstrip(",") == "b":
                reg = b

            if instruction == "hlf":
                reg = hlf(reg)
            elif instruction == "tpl":
                reg = tpl(reg)
            elif instruction == "inc":
                reg = inc(reg)
            elif instruction == "jie":
                i = jie(reg, int(line[2].lstrip("+")), i)
                continue
            elif instruction == "jio":
                i = jio(reg, int(line[2].lstrip("+")), i)
                continue
            
            if line[1].rstrip(",") == "a":
                a = reg
            elif line[1].rstrip(",") == "b":
                b = reg

        i += 1

print "a final value: {}".format(a)
print "b final value: {}".format(b)
