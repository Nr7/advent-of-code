import itertools
shortest = -1
longest = -1
cities = {}

# Partially based on this TSP solution: http://codereview.stackexchange.com/questions/81865/travelling-salesman-using-brute-force-and-heuristics

class City:
    def __init__ (self, name):
        self.name = name
        self.distances = {}

    def addDestination(self, name, distance):
        if name not in self.distances:
            self.distances[name] = int(distance)

def getDistance(city1, city2):
    return city1.distances[city2.name]

with open('input.txt') as input:
    for line in input.readlines():
        line = line.rstrip("\n").split(" ")
        # Add new city to dictionary
        if line[0] not in cities:
            city = City(line[0])
            cities[line[0]] = city
        # Add destination too if it's new
        if line[2] not in cities:
            city = City(line[2])
            cities[line[2]] = city
        # Add distances to both cities
        cities[line[0]].addDestination(line[2], line[4])
        cities[line[2]].addDestination(line[0], line[4])

for tour in itertools.permutations(cities):
    distance = sum([getDistance(cities[city], cities[tour[index + 1]]) for index, city in enumerate(tour[:-1])])
    if (shortest == -1) or (distance < shortest):
        shortest = distance
    if distance > longest:
        longest = distance

print "Shortest route: " + str(shortest)
print "Longest route: "  + str(longest)
