ingredients = []
high_score = -1
high_score_cal = -1

class Ingredient:
    def __init__(self, name, capacity, durability, flavor, texture, calories):
        self.name = name
        self.capacity = capacity
        self.durability = durability
        self.flavor = flavor
        self.texture = texture
        self.calories = calories
        self.tea_spoons = 0

    def set_tea_spoons(self, tea_spoons):
        self.tea_spoons = tea_spoons

    def get_property(self, prop):
        if prop == "capacity":
            value = self.capacity
        elif prop == "durability":
            value = self.durability
        elif prop == "flavor":
            value = self.flavor
        elif prop == "texture":
            value = self.texture
        elif prop == "calories":
            value = self.calories
        else:
            return 0
        return value * self.tea_spoons

    def __str__(self):
        return "%s: %i, %i, %i, %i, %i" % (self.name, self.capacity, self.durability, self.flavor, self.texture, self.calories)

def calculate_score(): 
    global ingredients
    global high_score
    global high_score_cal

    capacity = max(0, sum(i.get_property("capacity") for i in ingredients))

    durability = max(0, sum(i.get_property("durability") for i in ingredients))
        
    flavor = max(0, sum(i.get_property("flavor") for i in ingredients))

    texture = max(0, sum(i.get_property("texture") for i in ingredients))
    # Calories are always >= 0
    calories = sum(i.get_property("calories") for i in ingredients)

    score = capacity * durability * flavor * texture
    high_score = max(high_score, score)

    if calories == 500:
        high_score_cal = max(high_score_cal, score)

def func(ings, left):
    if len(ings) == 1:
        ings[0].set_tea_spoons(left)
        calculate_score()
    else:
        for i in range(0, left):
            ings[0].set_tea_spoons(i)
            func(ings[1:], left-i)



with open('input.txt') as input:
    for line in input.readlines():
        line = line.split(" ")
        ing = Ingredient(line[0], int(line[2][:-1]), int(line[4][:-1]), int(line[6][:-1]), int(line[8][:-1]), int(line[10]))
        ingredients.append(ing)

func(ingredients, 100)

print high_score
print high_score_cal
