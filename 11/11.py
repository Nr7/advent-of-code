import re
bad_chars = ['i', 'o', 'l']
# ([a-z]) any single char (a group)
# (\2) the first group/character again
# [a-z]* zero or more of any character
# ((?!\2)[a-z]) any single char _except_ the first match (negative backreference (I think?))
# (\4) previous character/group again
double_char = re.compile("(([a-z])\\2)[a-z]*(((?!\\2)[a-z])\\4)")

def check_order(line):
    con_chars = 0
    prev_ch = ''
    for c in line:
        if not prev_ch:
            # First character
            prev_ch = c
            continue
        if ord(c) == ord(prev_ch)+1:
            con_chars += 1
            if con_chars == 2:
                # Pattern found
                return True
        else:
            con_chars = 0
        prev_ch = c
    return False

def increment_password(password):
    if password[-1] == "z":
        if len(password) == 1:  # Overflow handling
            return "a"
        else:
            password = increment_password(password[:-1]) + "a"
    else:
        # Skip 'i', 'o' & 'l' altogether since they are not allowed anyway
        temp = chr(ord(password[-1])+1)
        if temp in bad_chars:
            temp = chr(ord(temp)+1)
        #No need to recheck since the bad chars are not consecutive in the alphabet
        password = password[:-1] + temp
    return password

def find_next_password(password):
    while True:
        password = increment_password(password)
        if check_order(password):
            if not any (x in password for x in bad_chars): 
                if double_char.search(password):
                    # Good password found
                    return password

nice = 0
password = 'cqjxjnds'

password = find_next_password(password)
print password

password = find_next_password(password)
print password
