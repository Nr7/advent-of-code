def look_and_say(string):
    #print string
    output = ""
    count = 1
    current = string[0]
    for c in string[1:]:
        if c != current:
            output += str(count) + current
            current = c
            count = 1
        else:
            count += 1

    output += str(count) + current
    return output

string = "3113322113"
for i in range(0, 50):
    print i
    string = look_and_say(string)

print len(string)
