#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdint.h>
#include "hash.h"

#define BUF_SIZE    50
#define MAX_TOKENS  3

uint8_t u8_is_int(char *var)
{
    char *end = NULL;
    errno = 0;
    long temp = strtol(var, &end, 10);
    if ((end != var) && (errno != ERANGE) && (temp >= INT_MIN) && (temp <= INT_MAX))
    {
        return 1;
    }
    return 0;
}

char *p8_resolve_value(char *var)
{
    node_t *pt_n;
    // Check if the value is a raw integer (shouldn't happen?)
    if (u8_is_int(var))
    {
        return var;
    }

    // Check if we have this key in the hashtable
    if ((pt_n = lookup(var)) == NULL)
    {
        // ERROR
        return NULL;
    }

    // Resolve node value(s)
    switch (pt_n->value_count)
    {
        case 1:
            // Value already resolved, return it
            if (u8_is_int(pt_n->value[0]))
            {
                return pt_n->value[0];
            }
            else
            {
                char *temp = p8_resolve_value(pt_n->value[0]);
                // Replace string with resolved value
                free(pt_n->value[0]);
                pt_n->value[0] = strdup(temp);
                return pt_n->value[0];
            }
            break;

        case 2:
            {
                // This should be a NOT operation
                char *temp = p8_resolve_value(pt_n->value[1]);
                uint16_t temp_int = (uint16_t)atoi(temp);
                temp_int = ~temp_int;
                // Replace string with resolved value
                free(pt_n->value[0]);
                free(pt_n->value[1]);
                pt_n->value[1] = NULL;
                pt_n->value_count = 1;
                // values are 16 bit unsigned numbers so 6 digits should be enough
                pt_n->value[0] = (char*)malloc(sizeof(char)*6);
                // Should use snprintf instead
                sprintf(pt_n->value[0], "%u", temp_int);
                return pt_n->value[0];
            }
            break;

        case 3:
            {
                // Resolve operands
                char *op1 = p8_resolve_value(pt_n->value[0]);
                char *op2 = p8_resolve_value(pt_n->value[2]);
                uint16_t u16_op1 = (uint16_t)atoi(op1);
                uint16_t u16_op2 = (uint16_t)atoi(op2);

                // Check operation
                if (strcmp(pt_n->value[1], "AND") == 0)
                {
                    u16_op1 = u16_op1 & u16_op2;
                }
                else if (strcmp(pt_n->value[1], "OR") == 0)
                {
                    u16_op1 = u16_op1 | u16_op2;
                }
                else if (strcmp(pt_n->value[1], "LSHIFT") == 0)
                {
                    u16_op1 = u16_op1 << u16_op2;
                }
                else if (strcmp(pt_n->value[1], "RSHIFT") == 0)
                {
                    u16_op1 = u16_op1 >> u16_op2;
                }
                else
                {
                    // Error!
                    return NULL;
                }

                // Replace string with resolved value
                free(pt_n->value[0]);
                free(pt_n->value[1]);
                free(pt_n->value[2]);
                pt_n->value[1] = NULL;
                pt_n->value[2] = NULL;
                pt_n->value_count = 1;
                // values are 16 bit unsigned numbers so 6 digits should be enough
                pt_n->value[0] = (char*)malloc(sizeof(char)*6);
                // Should use snprintf instead
                sprintf(pt_n->value[0], "%u", u16_op1);
                return pt_n->value[0];
            }
            break;

        default:
            break;
    }
    // This should never happen(?)
    return NULL;
}

int main()
{
    uint8_t au8_buf[BUF_SIZE];
    FILE* input = fopen("input2.txt", "r");
    while(fgets(au8_buf, sizeof(au8_buf), input) != NULL)
    {
        char *as_strs[5];
        for (int i = 0; i < 5; i++)
        {
            as_strs[i] = 0;
        }
        
        // Reserve the first slot for the last item
        as_strs[1] = (char*)strtok(au8_buf, " ");
        for (int i = 2; i < 5; i++)
        {
            as_strs[i] = (char*)strtok(NULL, " ");
            if (strcmp(as_strs[i], "->") == 0)
            {
                // Remove "->" string from the array for clarity
                as_strs[i] = 0;
                // Read the final token and exit the loop
                as_strs[0] = (char*)strtok(NULL, " ");
                // Remove newline from the end
                as_strs[0][strlen(as_strs[0])-1] = 0;
                break;
            }
        }
        insert(as_strs[0], &as_strs[1]);
    }
    printf("%s", p8_resolve_value("a"));
    fclose(input);
    return 0;
}


