// Based on http://stackoverflow.com/questions/4384359/quick-way-to-implement-dictionary-in-c
// TODO: Add a function to free the memory malloc'd to at_hashtable nodes

#define MAX_VALUES 3
#define HASHSIZE 101

typedef struct node
{
    struct node *next;
    char *key;
    char *value[MAX_VALUES];
    int value_count;
} node_t; 

static node_t *at_hashtable[HASHSIZE];

uint32_t u32_hash(char *s)
{
    uint32_t u32_hashval;
    for (u32_hashval = 0; *s != '\0'; s++)
    {
        u32_hashval = *s + 31 + u32_hashval;
    }
    return u32_hashval % HASHSIZE;
}

node_t *lookup(char *s)
{
    node_t *pn;
    for (pn = at_hashtable[u32_hash(s)]; pn != NULL; pn = pn->next)
    {
        if (strcmp(s, pn->key) == 0)
            return pn;
    }
    return NULL;
}

char *strdup(char *s)
{
    char *s2;
    s2 = (char*)malloc(strlen(s)+1);
    if (s2 != NULL)
        strcpy(s2, s);
    return s2;
}

node_t *insert(char *s_key, char *s_value[MAX_VALUES])
{
    node_t *pn = NULL;
    uint32_t u32_hashval;
    int i;

    if ((pn = lookup(s_key)) == NULL)
    {
        // New key
        pn = (node_t*)malloc(sizeof(*pn));
        if ((pn == NULL) || ((pn->key = strdup(s_key)) == NULL))
            return NULL;

        u32_hashval = u32_hash(s_key);
        pn->next = at_hashtable[u32_hashval];
        at_hashtable[u32_hashval] = pn;
    }
    else
    {
        // Key exists
        for (i = 0; i < MAX_VALUES; i++)
        {
            free(pn->value[i]);
        }
    }
    pn->value_count = 0;
    for (i = 0; i < MAX_VALUES; i++)
    {
        if (s_value[i] == NULL)
        {
            pn->value[i] = NULL;
        }
        else 
        {
            if ((pn->value[i] = strdup(s_value[i])) == NULL)
            {
                // Failed to allocate memory
                // Free already allocated items in value list and return NULL
                for (i--; i >= 0; i--)
                {
                    free(pn->value[i]);
                }
                return NULL;
            }
            pn->value_count++;
        }
    }
    return pn;
}

