values = {}


def lookup(var):
    if var.isdigit():
        return int(var)

    if var in values:
        value = values[var]
    else:
        raise ValueError('Non existent value')

    # Check if we already have this value
    if isinstance(value, int) or isinstance(value, long):
        return value
    if len(value) == 1:
        if value[0].isdigit():  
            # Integer value
            return int(value[0])
        else:
            # Variable
            values[var] = lookup(value[0])
            return values[var]
    elif len(value) == 2:
        # This should be a NOT operation
        if value[0] == "NOT":
            values[var] = ~lookup(value[1]) & 0xFFFF
            return values[var]
    elif len(value) == 3:
        # Format:  operand1 operation operand2
        op1 = lookup(value[0])
        op2 = lookup(value[2])
        if value[1] == "AND":
            values[var] = op1 & op2
        elif value[1] == "OR":
            values[var] = op1 | op2
        elif value[1] == "LSHIFT":
            values[var] = op1 << op2
        elif value[1] == "RSHIFT":
            values[var] = op1 >> op2
        else:
            raise ValueError('Unexpected operator')
        return values[var]
    raise ValueError('Unexpected value')

with open('input2.txt') as input:
    lines = input.readlines()
    for l in lines:
        line = l.rstrip("\n").split(" -> ")
        values[line[1]] = line[0].split(" ")

print lookup("a")
#for key in sorted(values):
#    print "%s: %s" % (key, values[key])
