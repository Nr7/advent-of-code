import itertools
happiest = -1
happiestArrangement = []
people = {}

# Based on the TSP code in puzzle #9

class Person:
    def __init__ (self, name):
        self.name = name
        self.likes = {}

    def add_person(self, name, happiness):
        if name not in self.likes:
            self.likes[name] = int(happiness)

    def __str__(self):
        return self.name

    def __repr__(self):
        temp = self.name + " knows "
        for person in self.likes.keys():
            temp += person + ": " + str(self.likes[person]) + ", "
        return temp

def get_total_happiness(person1, person2):
    return person1.likes[person2.name] + person2.likes[person1.name]

with open('input.txt') as input:
    for line in input.readlines():
        line = line.rstrip(".\n").split(" ")
        # Add new person to dictionary
        if line[0] not in people:
            person = Person(line[0])
            people[line[0]] = person
        # Add minus sign is happiness is negative
        if line[2] == "lose":
            line[3] = "-" + line[3]
        # Add happiness towards other person
        people[line[0]].add_person(line[10], line[3])

def calculate_happiness(lst):
    # Generate a list containing the happinessess of each person pair
    happiness = sum([get_total_happiness(people[person], people[lst[index + 1]]) for index, person in enumerate(lst[:-1])])
    # Add happiness between first and last too
    happiness += get_total_happiness(people[lst[0]], people[lst[-1]])
    return happiness

for arrangement in itertools.permutations(people):
    happiness = calculate_happiness(arrangement)
    if happiness > happiest:
        happiest = happiness
        happiestArrangement = list(arrangement)

print happiestArrangement
print "Happiest arrangement without me: "  + str(happiest)

# Add myself to people
me = Person("Me")
for person in people:
    me.add_person(person, 0)
    people[person].add_person("Me", 0)
people["Me"] = me
# Recalculate happiness using the happiest arrangement as base and inserting me in different slots
happiest = 0
for i in range(0, len(happiestArrangement)):
    temp_arr = list(happiestArrangement)
    temp_arr.insert(i, me.name)
    happiness = calculate_happiness(temp_arr)
    if happiness > happiest:
        happiest = happiness

print "Happiest arrangement with me: "  + str(happiest)
