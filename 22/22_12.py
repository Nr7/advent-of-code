import random
from copy import deepcopy

# Set False for part 1, True for part 2
hard_mode = True

class Dude:
    def __init__(self, name, hp, dmg, arm):
        self.name = name
        self.max_hp = int(hp)
        self.hp =  int(hp)
        self.dmg = int(dmg)
        self.arm = int(arm)
        self.curr_arm = int(arm)
        self.effects = []

    def apply_effects(self):
        expired = []
        for effect in self.effects:
            if effect.effect_type[:3] == "dmg": # HP modifying effects
                if self.get_hit(effect.amount): # Check if we died because of the effect
                    return True
                if effect.increment():
                    expired.append(effect)
            elif effect.effect_type == "armor": # Armor value modifying effects
                if effect.increment():
                    self.curr_arm = self.arm
                    expired.append(effect)
                else:
                    self.curr_arm = effect.amount
            elif effect.effect_type[:4] == "mana": # Mana modifying effects
                self.curr_mana += effect.amount
                if effect.increment():
                    expired.append(effect)
        # Remove expired effects
        for effect in expired:
            self.effects.remove(effect)
        # Return False because we are still alive
        return False

    # Returns -1 if self dies
    #          1 if enemy dies
    #          0 if both live
    def act(self, enemy):
        # Apply effects and check if we died from them
        if self.apply_effects():
            return -1
        if enemy.apply_effects():
            return 1
        # Hit enemy
        if enemy.get_hit(self.dmg):
            return 1
        return 0

    def get_hit(self, hit):
        dmg_done = int(hit) - self.curr_arm
        dmg_done = max(dmg_done, 1)
        self.hp -= dmg_done
        #print "{} HP: {}".format(self.name, self.hp)
        return (self.hp <= 0)

    # Confusing name, should be get_hit_by_spell or something
    def cast_spell(self, effects):
        for effect in effects:
            if effect.effect_type == "dmg_instant":
                if self.get_hit(effect.amount): # Check if we died because of the effect
                    return True
            else:
                if effect not in self.effects:
                    self.effects.append(deepcopy(effect))
        return False

    def has_effect(self, effects):
        for effect in effects:
            if effect in self.effects:
                return True
        return False

    def reset(self):
        self.hp = self.max_hp
        self.effects = []
        self.curr_arm = self.arm

class Wizard(Dude):
    def __init__(self, name, hp, dmg, arm, mana):
        Dude.__init__(self, name, hp, dmg, arm)
        self.mana = int(mana)
        self.curr_mana = int(mana)
        self.used_mana = 0
        self.used_spells = []

    def reset(self):
        Dude.reset(self)
        self.curr_mana = self.mana
        self.used_mana = 0
        self.used_spells = []

    # Returns -1 if self dies
    #          1 if enemy dies
    #          0 if both live
    def act(self, enemy):
        global spells
        global hard_mode

        if hard_mode:
            if self.get_hit(1):
                return -1

        # Apply effects and check if we or the enemy died from them
        if self.apply_effects():
            return -1
        if enemy.apply_effects():
            return 1

        #charging = False
        ## Check if recharge is in effect
        #for effect in self.effects:
        #    if effect.effect_type == "mana":
        #        charging = True
        #        break
        #    
        #if not charging:
        #    # Check if we have enough mana to cast the spell with the lowest mana cost
        #    # (53, Magic Missile) and still have enough mana to cast Recharge (229)
        #    if (self.curr_mana - spells["Recharge"].mana_cost - spells["Magic Missile"].mana_cost) < spells["Recharge"].mana_cost:
        #        # Always cast "Recharge" when necessary
        #        #print "PC cast spell Recharge"
        #        self.curr_mana -= spells["Recharge"].mana_cost
        #        self.used_mana += spells["Recharge"].mana_cost
        #        self.used_spells.append(deepcopy(spells["Recharge"]))
        #        #print "PC mana: {}".format(self.curr_mana)
        #        self.cast_spell(spells["Recharge"].good_effects)
        #        # Just in case
        #        if self.curr_mana <= 0:
        #            return -1
        #        return 0

        # Cast a spell (that we can afford) by random
        while True:
            spell_int = random.randint(0,len(spells)-1)
            spell_name = spells.keys()[spell_int]
            spell = spells[spell_name]
            #spell = spells[spells.keys()[random.randint(0,len(spells)-1)]]
            if (self.curr_mana >= spell.mana_cost) and (not enemy.has_effect(spell.bad_effects)) and (not self.has_effect(spell.good_effects)):
                #print spell_int
                #print spell_name
                #print "PC cast spell {}".format(spell.name)
                self.curr_mana -= spell.mana_cost
                self.used_mana += spell.mana_cost
                self.used_spells.append(deepcopy(spell))
                #print "PC mana: {}".format(self.curr_mana)
                if enemy.cast_spell(spell.bad_effects):
                    return 1
                if self.cast_spell(spell.good_effects):
                    return -1
                break

        # If there isn't any mana left to cast any spells, we die
        if self.curr_mana <= spells["Magic Missile"].mana_cost:
            return -1
        return 0

class Effect:
    def __init__(self, duration, effect_type, amount):
        self.duration = int(duration)
        self.effect_type = effect_type
        self.amount = int(amount)

    def increment(self):
        self.duration -= 1
        return self.duration <= 0

    def __eq__(self, other):
        return (self.effect_type == other.effect_type) and (self.amount == other.amount)

    def __ne__(self, other):
        return not self.__eq__(self, other)

class Spell:
    def __init__(self, name, mana_cost, g_effects, b_effects):
        self.name = name
        self.mana_cost = mana_cost
        self.good_effects = g_effects
        self.bad_effects = b_effects

def fight(pc, boss):
    pc_won = False
    while True:
        #print "\n---- PC Turn ----"
        result = pc.act(boss)
        if result == -1:
            pc_won = False # Boss won :(
            break
        if result == 1:    
            pc_won = True  # Player won :)
            break

        #print "\n--- Boss Turn ---"
        result = boss.act(pc)
        if result == -1:
            pc_won = True  # Player won :)
            break
        if result == 1:    
            pc_won = False # Boss won :(
            break
    used_mana = pc.used_mana
    used_spells = deepcopy(pc.used_spells)
    pc.reset()
    boss.reset()
    return (pc_won, used_mana, used_spells)

spells = {}

pc = Wizard("PC", 50, 0, 0, 500)
boss = Dude("Boss", 71, 10, 0)

# Create spells
spells["Magic Missile"] = Spell("Magic Missile", 53, [], [Effect(0, "dmg_instant", 4)])
spells["Drain"] = Spell("Drain", 73, [Effect(0, "dmg_instant", -2)], [Effect(0, "dmg_instant", 2)])
spells["Shield"] = Spell("Shield", 113, [Effect(6, "armor", 7)], [])
spells["Poison"] = Spell("Poison", 173, [], [Effect(6, "dmg_heal", 3)])
spells["Recharge"] = Spell("Recharge", 229, [Effect(5, "mana", 101)], [])

manas = []

while True:
    pc_won, used_mana, used_spells = fight(pc, boss)
    if pc_won:# and used_mana < 2000:
        print "Player won"
        print "Used mana: {}".format(used_mana)
        print "Used spells: " + ', '.join(spell.name for spell in used_spells)
        manas.append(used_mana)
        if len(manas) >= 10:
            break
    #else:
    #    print "Boss won"

print "Least amount of mana used: {}".format(sorted(manas)[0])
