class Aunt:
    def __init__(self, input_list):
        self.properties = {}
        for line in input_list:
            line = line.rstrip("\n").split(" ")
            self.properties[line[0].rstrip(":")] = int(line[1])

    # Returns the number of the aunt if she matches. -1 if no match.
    def match(self, aunt):
        aunt = aunt.split(" ")
        # All aunts in the input have three properties
        if self.properties[aunt[2].rstrip(":")] == int(aunt[3].rstrip(",")):
            if self.properties[aunt[4].rstrip(":")] == int(aunt[5].rstrip(",")):
                if self.properties[aunt[6].rstrip(":")] == int(aunt[7].rstrip("\n")):
                    return int(aunt[1].rstrip(":"))
        return -1

sue = None
with open('sue.txt') as sue_input:
   sue = Aunt(sue_input.readlines())

if sue:
    with open('input.txt') as input:
        for line in input.readlines():
            number = sue.match(line)
            if number != -1:
                print number
                break
