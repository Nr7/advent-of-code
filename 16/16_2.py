class Aunt:
    def __init__(self, input_list):
        self.properties = {}
        for line in input_list:
            line = line.rstrip("\n").split(" ")
            self.properties[line[0].rstrip(":")] = int(line[1])

    def compare_property(self, name, value):
        # Check special cases first
        if (name == "cats") or (name == "trees"):
            if int(value) > self.properties[name]:
                return True
            else:
                return False
        elif (name == "pomeranians") or (name == "goldfish"):
            if int(value) < self.properties[name]:
                return True
            else:
                return False
        else:
            if self.properties[name] == int(value):
                return True
            else:
                return False

    # Returns the number of the aunt if she matches. -1 if no match.
    def match(self, aunt):
        aunt = aunt.split(" ")
        # All aunts in the input have three properties
        if self.compare_property(aunt[2].rstrip(":"), aunt[3].rstrip(",")):
            if self.compare_property(aunt[4].rstrip(":"), aunt[5].rstrip(",")):
                if self.compare_property(aunt[6].rstrip(":"), aunt[7].rstrip("\n")):
                    return int(aunt[1].rstrip(":"))
        return -1

sue = None
with open('sue.txt') as sue_input:
   sue = Aunt(sue_input.readlines())

if sue:
    with open('input.txt') as input:
        for line in input.readlines():
            number = sue.match(line)
            if number != -1:
                print number
                break

