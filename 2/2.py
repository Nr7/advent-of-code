totalArea = 0
ribbonLen = 0
with open('input.txt') as input:
    while True:
        line = input.readline()
        if not line:
            break
        dimensions = sorted(map(int, line.split("x"))) # Assume all lines have correct input
        if dimensions: 
            areas = []
            areas.append(dimensions[0] * dimensions[1])
            areas.append(dimensions[0] * dimensions[2])
            areas.append(dimensions[1] * dimensions[2])
            totalArea += areas[0]*2 + areas[1]*2 + areas[2]*2 + min(areas)
            
            ribbonLen += dimensions[0]*2 + dimensions[1]*2 + (dimensions[0] * dimensions[1] * dimensions[2])
        else:  
            print "ERROR"
            break

print totalArea
print ribbonLen 
