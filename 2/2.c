#include <stdio.h>
#include <stdint.h>
#include <string.h>

uint32_t min(uint32_t a, uint32_t b)
{
    if (a < b) return a;
    return b;
}

uint32_t max(uint32_t a, uint32_t b)
{
    if (a > b) return a;
    return b;
}

int main()
{
    uint32_t u32_totalArea = 0;
    uint32_t u32_ribbonLen = 0;
    uint8_t au8_buf[20];

    FILE* input = fopen("input.txt", "r");
    while(fgets(au8_buf, sizeof(au8_buf), input) != NULL)
    {
        // Assume correct input
        uint32_t edge1 = atoi(strtok(au8_buf, "x"));
        uint32_t edge2 = atoi(strtok(NULL, "x"));
        uint32_t edge3 = atoi(strtok(NULL, "x"));

        uint32_t face1 = edge1 * edge2;
        uint32_t face2 = edge1 * edge3;
        uint32_t face3 = edge2 * edge3;

        u32_totalArea += face1*2 + face2*2 + face3*2 + min(min(face1,face2),face3);
        u32_ribbonLen += min(edge1,edge2)*2 +
                         min(max(edge1,edge2),edge3)*2 + 
                         (edge1*edge2*edge3);
    }
    printf("%u square feet of paper\n", u32_totalArea);
    printf("%u feet of ribbon\n", u32_ribbonLen);
    fclose(input);
    return 0;
}


