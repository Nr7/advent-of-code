import itertools
containers = []
combinations = 0 
current_count = 0
smallest_comb = 1000
smallest_comb_count = 0

class Container:
    def __init__(self, _id, volume):
        self._id = _id
        self.volume = volume

def fun(containers, eggnog):
    global combinations
    global current_count
    global smallest_comb
    global smallest_comb_count

    if containers[0].volume <= eggnog:
        new_eggnog = eggnog - containers[0].volume
        current_count += 1
        if new_eggnog == 0:
            combinations += 1
            if current_count < smallest_comb:
                smallest_comb = current_count
                smallest_comb_count = 1
            elif current_count == smallest_comb:
                smallest_comb_count += 1

        elif len(containers) > 1:
            fun(containers[1:], new_eggnog)
        current_count -= 1

    if len(containers) > 1:
        fun(containers[1:], eggnog)
    return


with open('input.txt') as input:
    for i, line in enumerate(input.readlines()):
        cont = Container(i, int(line))
        containers.append(cont)

fun(containers, 150)
print combinations
print "Smallest combination: %i" % (smallest_comb)
print "Amount of smallest combinations: %i" % (smallest_comb_count)
