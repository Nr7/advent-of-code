import itertools
containers = []
combinations = 0

class Container:
    def __init__(self, _id, volume):
        self._id = _id
        self.volume = volume

def fun(containers, eggnog):
    global combinations
    if containers[0].volume <= eggnog:
        new_eggnog = eggnog - containers[0].volume
        if new_eggnog == 0:
            combinations += 1
        elif len(containers) > 1:
            fun(containers[1:], new_eggnog)

    if len(containers) > 1:
        fun(containers[1:], eggnog)
    return


with open('input.txt') as input:
    for i, line in enumerate(input.readlines()):
        cont = Container(i, int(line))
        containers.append(cont)

fun(containers, 150)
print combinations
