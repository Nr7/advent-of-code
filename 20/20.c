#include <stdio.h>
#include <stdint.h>

#define TARGET 34000000u
#define HOUSES 900000u
#define START 1u

int main()
{
    uint32_t *houses;
    houses = (uint32_t*)calloc(HOUSES, sizeof(uint32_t));

    for(uint32_t elf = 1; elf < HOUSES; elf++)
    {
        for(uint32_t house = elf; house < HOUSES; house += elf)
        {
            houses[house] += elf * 10;
        }
    }

    for(uint32_t house = 1; house < HOUSES; house++)
    {
        if (houses[house] >= TARGET)
        {
            printf("%u presents delivered to house %u\n", houses[house], house);
            break;
        }
    }

    for(uint32_t elf = 1; elf < HOUSES; elf++)
    {
        uint8_t count = 0;
        for(uint32_t house = elf; house < HOUSES; house += elf)
        {
            houses[house] += elf * 11;
            if (++count >= 50)
                break;
        }
    }

    for(uint32_t house = 1; house < HOUSES; house++)
    {
        if (houses[house] >= TARGET)
        {
            printf("%u presents delivered to house %u\n", houses[house], house);
            break;
        }
    }
    free(houses);
    return 0;
}
