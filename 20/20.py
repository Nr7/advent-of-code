from collections import defaultdict

target = 34000000
houses = 1000000

presents = defaultdict(int)

for elf in xrange(1, houses):
    for house in xrange(elf, houses, elf):
        presents[house] += elf * 10


for house in xrange(1, houses):
    if presents[house] >= target:
        print "{0} presents delivered in house {1}".format(presents[house], house) 
        break

presents2 = defaultdict(int)
for elf in xrange(1, houses):
    count = 0
    for house in xrange(elf, houses, elf):
        presents2[house] += elf * 11
        count += 1
        if count >= 50:
            break

for house in xrange(1, houses):
    if presents2[house] >= target:
        print "{0} presents delivered in house {1}".format(presents2[house], house) 
        break
