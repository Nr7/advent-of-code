import re
# ([a-z][a-z]) any two characters (a group)
# [a-z]* zero or more of any character
# \1 the first group of two matched characters again
doubleChar = re.compile("([a-z][a-z])[a-z]*\\1")
# ([a-z]) any character (a group)
# [a-z] any character
# \1 the first matched character group again
tripleChar = re.compile("([a-z])[a-z]\\1")

nice = 0
with open('input.txt') as input:
    for line in input.readlines():
        if doubleChar.search(line):
            if tripleChar.search(line):
                print line
                nice += 1

print nice
