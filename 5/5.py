import re
badStrings = ["ab", "cd", "pq", "xy"]
#string = "haegwjzuvuyypxyu"
#string = "ugknbfddgicrmopn"
#string = "aaa"
#string = "dvszwmarrgswjxmb"

# ([a-z]) any character
# \1 the first matched character group again
doubleChar = re.compile("([a-z])\\1")
# ([~aeiou]* zero or more (english) non-vowel characters
# [aeiou]) a single (english) vowel character
# {3} previous times three
# [~aeiou]* zero or more (english) non-vowel characters
# $ end of string (unnecessary?)
tripleChar = re.compile("([^aeiou]*[aeiou]){3}[^aeiou]*$")
nice = 0
with open('input.txt') as input:
    while True:
        line = input.readline()
        if not line:
            break

        if not any (x in line for x in badStrings): 
            if doubleChar.search(line):
                if tripleChar.search(line):
                    nice += 1

print nice
