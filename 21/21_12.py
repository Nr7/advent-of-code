class Equipment:
    def __init__(self, name, cost, damage, armor):
        self.name = name
        self.cost = int(cost)
        self.damage = int(damage)
        self.armor = int(armor)

class Dude:
    def __init__(self, name, hp, dmg, arm):
        self.name = name
        self.max_hp = int(hp)
        self.hp =  int(hp)
        self.dmg = int(dmg)
        self.arm = int(arm)

    def get_hit(self, hit):
        dmg_done = int(hit) - self.arm
        dmg_done = max(dmg_done, 1)
        self.hp -= dmg_done
        #print "{} HP: {}".format(self.name, self.hp)
        return (self.hp <= 0)

    def reset(self):
        self.hp = self.max_hp

def fight(pc, boss):
    pc_won = False
    while True:
        if boss.get_hit(pc.dmg):
            pc_won = True # Player won :)
            break
        if pc.get_hit(boss_dmg):
            pc_won = False # Boss won :(
            break
    pc.reset()
    boss.reset()
    return pc_won

weapons = []
armors = []
rings = []
eqpmt = {}
pc = Dude("PC", 100, 0, 0)
boss = None

with open('store.txt') as store:
    lines = store.readlines()
    i = 1
    while True:
        if lines[i] == "\n":
            break
        line = lines[i].rstrip("\n").split()
        weapons.append(Equipment(line[0], line[1], line[2], line[3]))
        i += 1
        
    i += 2
    armors.append(Equipment("None", 0, 0, 0))
    while True:
        if lines[i] == "\n":
            break
        line = lines[i].rstrip("\n").split()
        armors.append(Equipment(line[0], line[1], line[2], line[3]))
        i += 1
            
    i += 2
    rings.append(Equipment("None", 0, 0, 0))
    while i < len(lines):
        line = lines[i].rstrip("\n").split()
        rings.append(Equipment(line[0] + " " + line[1], line[2], line[3], line[4]))
        i += 1

with open('input.txt') as b:
    lines = b.readlines()
    boss_hp = lines[0].rstrip("\n").split(" ")[2]
    boss_dmg = lines[1].rstrip("\n").split(" ")[1]
    boss_arm = lines[2].rstrip("\n").split(" ")[1]
    boss = Dude("Boss", boss_hp, boss_dmg, boss_arm)

# List all possible equipment combinations
for wp in weapons:
    for ar in armors:
        for rn1 in rings:
            for rn2 in rings:
                if rn1 is rn2:
                    continue
                else:
                    lst = [wp, ar, rn1, rn2]
                    cost = sum(x.cost for x in lst)
                    eqpmt[cost] = lst

for cost in sorted(eqpmt.keys()):
    pc.dmg = eqpmt[cost][0].damage
    pc.arm = eqpmt[cost][1].armor
    pc.dmg += eqpmt[cost][2].damage
    pc.arm += eqpmt[cost][2].armor
    if fight(pc, boss):
        print "Player wins!"
        print "Winning equipment"
        print "Weapon: {}".format(eqpmt[cost][0].name)
        print "Armor: {}".format(eqpmt[cost][1].name)
        print "Ring 1: {}".format(eqpmt[cost][2].name)
        print "Ring 2: {}".format(eqpmt[cost][3].name)
        print "Cost: {}".format(cost)
        break
    #else:
    #    print "Boss wins"

print ""

for cost in reversed(sorted(eqpmt.keys())):
    pc.dmg = eqpmt[cost][0].damage
    pc.arm = eqpmt[cost][1].armor
    pc.dmg += eqpmt[cost][2].damage
    pc.arm += eqpmt[cost][2].armor
    if not fight(pc, boss):
        print "Boss wins"
        print "Losing equipment"
        print "Weapon: {}".format(eqpmt[cost][0].name)
        print "Armor: {}".format(eqpmt[cost][1].name)
        print "Ring 1: {}".format(eqpmt[cost][2].name)
        print "Ring 2: {}".format(eqpmt[cost][3].name)
        print "Cost: {}".format(cost)
        break
