generated_mols = []
input_mols = {}
start_mol = ""

def generate_mols(start_mol, input_mols):
    global generated_mols
    temp = ""
    for i, c in enumerate(start_mol):
        if c.isupper():
            temp = c
        else:
            temp = temp + c
        if temp in input_mols:
            for mol in input_mols[temp]:
                print temp
                if len(temp) == 2:
                    new_mol = start_mol[:i-1] + mol + start_mol[i+1:]
                    print new_mol
                else:   # Should be 1
                    new_mol = start_mol[:i] + mol + start_mol[i+1:]
                if new_mol not in generated_mols:
                    generated_mols.append(new_mol)
            temp = ""
        

with open('input.txt') as data:
    for i, line in enumerate(data.readlines()):
        words = line.rstrip("\n").split(" => ")
        if len(words) == 2:
            #print words
            if words[0] not in input_mols:
                temp_list = []
                input_mols[words[0]] = temp_list
            input_mols[words[0]].append(words[1])
        else:
            # Last row contains the starting molecule
            # Second to last row is empty
            if line:
                start_mol = line.rstrip("\n")


generate_mols(start_mol, input_mols)

print len(generated_mols)
