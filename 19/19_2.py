input_mols = {}
start_mol = ""
target_mol = "e"
min_steps = 0

def find_mol(start_mol, input_mols, target_mol, steps):
    global min_steps
    while start_mol != target_mol:
        for key in reversed(sorted(input_mols.keys(), key=len)):
            if key in start_mol:
                print start_mol
                start_mol = start_mol.replace(key, input_mols[key], 1)
                steps += 1

    return steps
     

with open('input.txt') as data:
    for i, line in enumerate(data.readlines()):
        words = line.rstrip("\n").split(" => ")
        if len(words) == 2:
            input_mols[words[1]] = words[0]
        else:
            # Last row contains the target molecule
            # We'll use that as the start because we do this backwards
            # Second to last row is empty
            if line:
                start_mol = line.rstrip("\n")


print find_mol(start_mol, input_mols, target_mol, 0)
