#include "stdio.h"
#include "stdint.h"
#include "malloc.h"

#define GRID_WIDTH 1000
#define START_COORD (GRID_WIDTH/2)
int main()
{
    uint32_t x1 = START_COORD;
    uint32_t y1 = START_COORD;
    uint32_t x2 = START_COORD;
    uint32_t y2 = START_COORD;
    uint32_t *x, *y;
    uint8_t u8_turn = 0;
    uint32_t u32_houses_visited = 0;
    // Dumb solution, but works for this purpose
    // Allocate (hopefully) enough space so we don't go over and start from the middle
    // Memory's cheap nowadays, right? :)
    uint8_t **au8_houses = (uint8_t**)calloc(GRID_WIDTH, sizeof(uint8_t*));
    if (au8_houses == NULL) 
    {
        return -1;
    }

    for (int i = 0; i < GRID_WIDTH; i++)
    {
        au8_houses[i] = (uint8_t*)calloc(GRID_WIDTH, sizeof(uint8_t*));
        if (au8_houses[i] == NULL) 
        {
            return -1;
        }
    }
    FILE* input = fopen("input.txt", "r");
    char ch = fgetc(input);
    while(ch != EOF)
    {
        if (u8_turn == 0)
        {
            x = &x1;
            y = &y1;
            u8_turn = 1;
        }
        else
        {
            x = &x2;
            y = &y2;
            u8_turn = 0;
        }
        switch (ch)
        {
            case '^':
                (*y)++;
                break;

            case 'v':
                (*y)--;
                break;

            case '<':
                (*x)--;
                break;

            case '>':
                (*x)++;
                break;

            default:
                break;
        }

        if ((*y < 0) || (*y >= GRID_WIDTH) || (*x < 0) || (*x >= GRID_WIDTH))
        {
            printf("ERROR, grid too small. Aborting\n");
            printf("x: %u, y:%u\n", *x, *y);
            fflush(stdout);
            break;
        }
        else
        {
            au8_houses[*x][*y] += 1;
        }
        ch = fgetc(input);
    }

    for (x1 = 0; x1 < GRID_WIDTH; x1++)
    {
        for (y1 = 0; y1 < GRID_WIDTH; y1++)
        {
            if (au8_houses[x1][y1] > 0)
            {
                u32_houses_visited++;
            }
        }
        free(au8_houses[x1]);
    }
    free(au8_houses);
    printf("%u houses visited\n", u32_houses_visited);
    fclose(input);
    
    return 0;
}




