#include "stdio.h"
#include "stdint.h"
#include "malloc.h"

#define GRID_WIDTH 1000
#define START_COORD (GRID_WIDTH/2)
int main()
{
    uint32_t x = START_COORD;
    uint32_t y = START_COORD;
    uint32_t u32_houses_visited = 0;
    // Dumb solution, but works for this purpose
    // Allocate (hopefully) enough space so we don't go over and start from the middle
    // Memory's cheap nowadays, right? :)
    uint8_t **au8_houses = (uint8_t**)calloc(GRID_WIDTH, sizeof(uint8_t*));
    if (au8_houses == NULL) 
    {
        return -1;
    }

    for (int i = 0; i < GRID_WIDTH; i++)
    {
        au8_houses[i] = (uint8_t*)calloc(GRID_WIDTH, sizeof(uint8_t*));
        if (au8_houses[i] == NULL) 
        {
            return -1;
        }
    }
    FILE* input = fopen("input.txt", "r");
    char ch = fgetc(input);
    while(ch != EOF)
    {
        switch (ch)
        {
            case '^':
                y++;
                break;

            case 'v':
                y--;
                break;

            case '<':
                x--;
                break;

            case '>':
                x++;
                break;

            default:
                break;
        }

        if ((y < 0) || (y >= GRID_WIDTH) || (x < 0) || (x >= GRID_WIDTH))
        {
            printf("ERROR, grid too small. Aborting\n");
            printf("x: %u, y:%u\n", x, y);
            fflush(stdout);
            break;
        }
        else
        {
            au8_houses[x][y] += 1;
        }
        ch = fgetc(input);
    }

    for (x = 0; x < GRID_WIDTH; x++)
    {
        for (y = 0; y < GRID_WIDTH; y++)
        {
            if (au8_houses[x][y] > 0)
            {
                u32_houses_visited++;
            }
        }
        free(au8_houses[x]);
    }
    free(au8_houses);
    printf("%u houses visited\n", u32_houses_visited);
    fclose(input);
    
    return 0;
}



