x = 0
y = 0
houses = {(x,y) : 1}
with open("input.txt") as input:
    while True:
        c = input.read(1)
        if not c:
            break;
        if   c == "^" : y += 1
        elif c == "<" : x -= 1
        elif c == ">" : x += 1
        elif c == "v" : y -= 1
        if (x,y) in houses:
            houses[(x,y)] += 1
        else:
            houses[(x,y)] = 1

#print houses
print len(houses)
