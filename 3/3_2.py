x1 = 0
y1 = 0
x2 = 0
y2 = 0
i  = 0
houses = {(x1,y1) : 1}

with open("input.txt") as input:
    while True:
        c = input.read(1)
        if not c:
            break
        if i == 0:
            i = 1
            if   c == "^" : y1 += 1
            elif c == "<" : x1 -= 1
            elif c == ">" : x1 += 1
            elif c == "v" : y1 -= 1
            if (x1,y1) in houses:
                houses[(x1,y1)] += 1
            else:
                houses[(x1,y1)] = 1
        else:
            i = 0
            if   c == "^" : y2 += 1
            elif c == "<" : x2 -= 1
            elif c == ">" : x2 += 1
            elif c == "v" : y2 -= 1
            if (x2,y2) in houses:
                houses[(x2,y2)] += 1
            else:
                houses[(x2,y2)] = 1

#print houses
print len(houses)

