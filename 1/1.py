floor = 0
pos = 1
with open('input.txt') as input:
    while True:
        c = input.read(1)
        if not c:
            # EOL
            break
        if c == "(":
            floor += 1
        elif c == ")":
            floor -= 1
        if floor is -1:
            print "Entered floor -1 at position: " + str(pos)
        pos += 1

print floor
