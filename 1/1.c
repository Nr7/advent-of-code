
#include "stdio.h"
#include "stdint.h"

int main()
{
    uint32_t u32_floor = 0;
    uint32_t u32_position = 1;
    FILE* input = fopen("input.txt", "r");
    char ch = fgetc(input);
    while(ch != EOF)
    {
        printf("%c\n", ch);
        switch (ch)
        {
            case '(':
                u32_floor++;
                break;

            case ')':
                u32_floor--;
                break;

            default:
                break;
        }
        if (u32_position > 0)
        {
            if (u32_floor == -1)
            {
                printf("Entered floor -1 at position: %u\n", u32_position);
                u32_position = -1;
            }
            else
            {
                u32_position++;
            }
        }
        ch = fgetc(input);
    }
    printf("Final floor: %u\n", u32_floor);
    fclose(input);
    return 0;
}
