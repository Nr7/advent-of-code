light_grid = []
class Light:
    def __init__(self, state):
        self.state = state
        self.next_state = ""

    def __nonzero__(self):
        if self.state == "#":
            return True
        else:
            return False

    def __str__(self):
        return self.state

    def advance_state(self):
        if self.next_state in ["#", "."]:
            self.state = self.next_state
        self.next_state = ""

    def turn_on(self):
        self.next_state = "#"

    def turn_off(self):
        self.next_state = "."

def count_surrounding_states(x,y):
    global light_grid
    if x > 0:
        lowx = x - 1
    else:
        lowx = x
    if x < len(light_grid)-1:
        highx = x + 1
    else:
        highx = x

    if y > 0:
        lowy = y - 1
    else:
        lowy = y
    if y < len(light_grid[x])-1:
        highy = y + 1
    else:
        highy = y

    count = 0
    for i in range(lowx, highx+1):
        for j in range(lowy, highy+1):
            # Skip the light itself
            if (i == x) and (j == y):
                continue
            if light_grid[i][j]:
                count += 1

    return count

def animate_lights(steps):
    global light_grid
    for x, array in enumerate(light_grid):
        for y, light in enumerate(array):
            # Corners stay always on
            if ((x == 0) and (y == 0)) or \
               ((x == len(array)-1) and (y == 0)) or \
               ((x == 0) and (y == len(light_grid)-1)) or \
               ((x == len(array)-1) and (y == len(light_grid)-1)):
                   continue
            count = count_surrounding_states(x,y)
            if light:
                if (count != 2) and (count != 3):
                    light.turn_off()
            else:
                if count == 3:
                    light.turn_on()


    # Advance the state of all lights
    for x, array in enumerate(light_grid):
        for y, light in enumerate(array):
            light.advance_state()

    #print_lights()

    if steps > 1:
        steps -= 1
        animate_lights(steps)

def print_lights():
    global light_grid
    for array in light_grid:
        light_str = ""
        for light in array:
            light_str += light.state
        print light_str
    print ""

def total_lights_on(light_grid):
    count = 0
    for array in light_grid:
        for light in array:
            if light:
                count += 1
    return count

with open('input2.txt') as data:
    for line in data.readlines():
        line = line.rstrip("\n")
        light_array = []
        light_grid.append([Light(ch) for ch in line])

#print_lights()

animate_lights(100)

print total_lights_on(light_grid)

