import re
count = 0
with open('input.txt') as input:
    lines = input.readlines()
    for line in lines:
        #print line
        # -*  == 0 or more minus signs
        # \d+ == 1 or more digits
        count += sum(map(int, re.findall('-*\d+', line)))
        #print count

print count
