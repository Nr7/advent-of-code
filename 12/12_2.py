import re, json

def parseJson(json):
    if   type(json) is list:
        return parseList(json)
    elif type(json) is dict:
        return parseDict(json)
    elif (type(json) is str) or (type(json) is unicode):
        # Ignore individual strings (Could we check for "red" here to avoid checking strings in two places?)
        return 0 
    elif type(json) is int:
        return json
    return 0

def parseList(lst):
    tempCount = 0
    for item in lst:
        tempCount += parseJson(item)
    return tempCount

def parseDict(dct):
    tempCount = 0
    # Check for "red" values and ignore the whole dictionary if found
    for v in dct.values():
        if v == "red":
            return 0
        else:
            tempCount += parseJson(v)
    return tempCount

with open('input.txt') as input:
    json_data = json.load(input)
    count = parseJson(json_data)

print count
