rawLength = 0
handledLength = 0
with open('input.txt') as input:
    for line in input.readlines():
        line = line.rstrip("\n")
        rawLength += len(line)
        print line
        line = line[1:]
        line = line[:-1]
        # Decode escape sequences
        line = line.decode('string_escape')
        print line
        handledLength += len(line)

print rawLength - handledLength
