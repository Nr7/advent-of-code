#include <stdio.h>
#include <stdint.h>
#include <string.h>

#define BUF_SIZE 100

int main()
{
    uint32_t u32_rawlength = 0;
    uint32_t u32_decodedLength = 0;
    uint32_t u32_reEncodedLength = 0;
    uint8_t au8_buf[BUF_SIZE];
    uint8_t au8_buf2[BUF_SIZE];
    FILE* input = fopen("input.txt", "r");
    while(fgets(au8_buf, sizeof(au8_buf), input) != NULL)
    {
        // Remove newline
        strtok(au8_buf, "\n");
        u32_rawlength += strlen(au8_buf);
        // Make a copy of the string and remove surrounding quotes
        strncpy(au8_buf2, &au8_buf[1], strlen(au8_buf)-2);
        // Add null termination
        au8_buf2[strlen(au8_buf)-2] = 0;
        // Count new length while decoding the escape sequences
        //printf("%s\n", au8_buf2);
        for (int i = 0; i < strlen(au8_buf2); i++)
        {
            switch(au8_buf2[i])
            {
                // Start of escape sequence
                case '\\':
                    // Special handling for '\x'
                    // Is followed by 2 hex characters
                    if (au8_buf2[i+1] == 'x')
                    {
                        i += 2;
                    }
                    // Skip next char
                    i++;
                    u32_decodedLength++;
                    break;

                default:
                    u32_decodedLength++;
                    break;
            }
        }

        // Encode the original string
        memset(au8_buf2, 0, BUF_SIZE);  // Clear au8_buf2 for shits and giggles
        // Start with a '"'
        int j = 0;
        au8_buf2[j++] = '"';
        for (int i = 0; i < strlen(au8_buf); i++)
        {
            //printf("j:%u\n", j);
            switch(au8_buf[i])
            {
                case '"':
                case '\\':
                    au8_buf2[j++] = '\\';
                // Intentional fall-through
                default:
                    au8_buf2[j++] = au8_buf[i];
                    break;
            }
        }
        // End with a '"'
        au8_buf2[j++] = '"';
        // (And a '\0')
        au8_buf2[j] = 0;

        u32_reEncodedLength += strlen(au8_buf2);
    }

    printf("Total raw length: %u\n", u32_rawlength);
    printf("Total decoded length: %u\n", u32_decodedLength);
    printf("Total re-encoded length: %u\n", u32_decodedLength);
    printf("Answer 1: %u\n", u32_rawlength-u32_decodedLength);
    printf("Answer 2: %u\n", u32_reEncodedLength-u32_rawlength);
    return 0;
}

