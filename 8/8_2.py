rawLength = 0
handledLength = 0
with open('input.txt') as input:
    for line in input.readlines():
        line = line.rstrip("\n")
        rawLength += len(line)
        print line
        # (I feel there must be a simpler way of doing this)
        # Add escape characters to the escape characters
        line = line.replace("\\", "\\\\")
        line = line.replace("\"", "\\\"")
        # Add new surrounding quotes
        line  = "\"" + line + "\""
        print line
        handledLength += len(line)

print handledLength - rawLength
