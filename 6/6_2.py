lights = [[0 for x in range(1000)] for x in range(1000)]

def modifyLights(point1, point2, command):
    x = int(point1[0])
    y = int(point1[1])
    while x <= int(point2[0]):
        #print "x:" + str(y)
        while y <= int(point2[1]):
            #print "y:" + str(y)
            if command == "toggle":
                lights[x][y] += 2
            elif command == "on":
                lights[x][y] += 1
            elif command == "off":
                if lights[x][y] > 0:
                    lights[x][y] -= 1
            y += 1
        x += 1
        y = int(point1[1])

def printLightsTotalBrightness():
    x = 0
    y = 0
    brightness = 0
    while x <= 999:
        while y <= 999:
            brightness += lights[x][y]
            y += 1
        x += 1
        y = 0
    print brightness

with open('input.txt') as input:
    while True:
        line = input.readline()
        if not line:
            break
        # Assume correct input
        words = line.split(" ")
        if words[0] == "toggle":
            command = words[0]
            point1 = words[1].split(",")
            point2 = words[3].split(",")
        elif words[0] == "turn":
            command = words[1]
            point1 = words[2].split(",")
            point2 = words[4].split(",")
        #print point1
        #print point2
        modifyLights(point1, point2, command)
    printLightsTotalBrightness()
