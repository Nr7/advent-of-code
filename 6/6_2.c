#include <stdio.h>
#include <stdint.h>

#define BUF_SIZE    40
#define LIGHTS_SIZE 1000
#define TOGGLE      0
#define TURN_ON     1
#define TURN_OFF    2

void modifyLights(uint8_t au8_lights[LIGHTS_SIZE][LIGHTS_SIZE], uint8_t u8_cmd, 
        uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
    for (uint16_t x = x1; x <= x2; x++)
    {
        for (uint16_t y = y1; y <= y2; y++)
        {
            switch(u8_cmd)
            {
                case TOGGLE:
                    au8_lights[x][y] += 2;
                    break;

                case TURN_ON:
                    au8_lights[x][y] += 1;
                    break;

                case TURN_OFF:
                    if (au8_lights[x][y] > 0)
                    {
                        au8_lights[x][y] -= 1;
                    }
                    break;

                default:
                    break;
            }
        }
    }
}

void printTotalBrightness(uint8_t au8_lights[LIGHTS_SIZE][LIGHTS_SIZE])
{
    uint32_t u32_brightness = 0;
    for (uint16_t x = 0; x < LIGHTS_SIZE; x++)
    {
        for (uint16_t y = 0; y < LIGHTS_SIZE; y++)
        {
            u32_brightness += au8_lights[x][y];
        }
    }
    printf("total brightness: %u\n", u32_brightness);
}

int main()
{
    uint8_t u8_cmd = 0xFF;
    uint16_t x1 = 0, y1 = 0;
    uint16_t x2 = 0, y2 = 0;
    uint8_t au8_buf[BUF_SIZE];
    uint8_t au8_lights[LIGHTS_SIZE][LIGHTS_SIZE] = {{0}};
    FILE* input = fopen("input.txt", "r");
    while(fgets(au8_buf, sizeof(au8_buf), input) != NULL)
    {
        if (strcmp(strtok(au8_buf, " "), "toggle") == 0)
        {
            u8_cmd = TOGGLE;
        }
        else
        {
            if (strcmp(strtok(NULL, " "), "on") == 0)
            {
                u8_cmd = TURN_ON;
            }
            else //if (strcmp(strtok(NULL, " ")), "off")
            {
                u8_cmd = TURN_OFF;
            }
        }

        x1 = atoi(strtok(NULL, ","));
        y1 = atoi(strtok(NULL, " "));

        // Skip the next word "through"
        strtok(NULL, " ");

        x2 = atoi(strtok(NULL, ","));
        y2 = atoi(strtok(NULL, " "));

        modifyLights(au8_lights, u8_cmd, x1, y1, x2, y2);
    }

    printTotalBrightness(au8_lights);
    fclose(input);
    return 0;
}

