lights = [[False for x in range(1000)] for x in range(1000)]

def modifyLights(point1, point2, command):
    x = int(point1[0])
    y = int(point1[1])
    while x <= int(point2[0]):
        #print "x:" + str(y)
        while y <= int(point2[1]):
            #print "y:" + str(y)
            if command == "toggle":
                lights[x][y] = not lights[x][y]
            elif command == "on":
                lights[x][y] = True
            elif command == "off":
                lights[x][y] = False
            y += 1
        x += 1
        y = int(point1[1])

def printLightsOnCount():
    x = 0
    y = 0
    count = 0
    while x <= 999:
        while y <= 999:
            if lights[x][y]:
                count += 1
            y += 1
        x += 1
        y = 0
    print count

with open('input.txt') as input:
    while True:
        line = input.readline()
        if not line:
            break
        # Assume correct input
        words = line.split(" ")
        if words[0] == "toggle":
            command = words[0]
            point1 = words[1].split(",")
            point2 = words[3].split(",")
        elif words[0] == "turn":
            command = words[1]
            point1 = words[2].split(",")
            point2 = words[4].split(",")
        #print point1
        #print point2
        modifyLights(point1, point2, command)
    printLightsOnCount()
